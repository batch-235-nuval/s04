const { assert } = require("chai");
const { getCircleArea } = require("../index.js");

describe("Test get circle area", () => {

	it("Test area of circle with radius 15 is 706.86", () => {

		let area = getCircleArea(15);
		assert.equal(area, 706.86);
	})

	it("Test area of circle with negative radius is undefined", () => {

		let area = getCircleArea(-1);
		assert.equal(area, undefined);
	})

	it("Test area of circle with 0 radius is undefined", () => {

		let area = getCircleArea(0);
		assert.isUndefined(area);
	})

	/*
		Mini-Activity: 10 mins

		Using the tdd process add another failing test to test a scenario if the user
		enters data that is not a number.
			-Assert the area to be undefined.
		Refactor the getCircleArea method to accomodate the scenario.

		Send your failing test and passing test in Hangouts
	*/


	it("Test area of circle if radius is string return undefined", () => {

		let area = getCircleArea("25");
		assert.isUndefined(area);
	})

})
